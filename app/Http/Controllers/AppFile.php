<?php

namespace App\Http\Controllers;

use App\UploadFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class AppFile extends Controller
{
    public function upload(Request $request) {
        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $ext = $request->file->extension();
            $allowedExt = explode(',',env('ALLOWED_EXT', 'docx,pdf,jpg,png'));
            if(in_array($ext,$allowedExt)) {
                $path = $request->file->store('files');
                $uuid = explode('.',basename($path))[0];

                $upload = new UploadFile();
                $upload->upload_path = $path;
                $upload->uuid =$uuid;
                $upload->file_name =  $request->file->getClientOriginalName();
                $upload->extension = $ext;
                $upload->save();

                return response()->json([
                    'uuid' => $uuid,
                    'link' => url("download/{$uuid}")
                ], 200);
            }
            return response()->json('Extension is not allowed', 400);
        }

        return response()->json('Invalid file uploaded', 400);
    }


    public function download($uuid) {
        $uploadedFile = UploadFile::where('uuid', '=', $uuid)->first();
        if($uploadedFile) {
            $toDate = strtotime('NOW');


            $expireDate = strtotime($uploadedFile->created_at. ' + '.env('LINK_EXPIRE', 10). ' days');

            if($toDate > $expireDate) {
                return response('Link expired', 403);
            }

            //dd($uploadedFile);
            $headers = array(
                'Content-Type: application/'.$uploadedFile->extension,
            );

            return Response::download(storage_path('app/'.$uploadedFile->upload_path), $uploadedFile->file_name, $headers);

        }

        return response('File not found', 404);
    }
}
