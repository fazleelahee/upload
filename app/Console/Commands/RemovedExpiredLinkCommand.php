<?php

namespace App\Console\Commands;

use App\UploadFile;
use Illuminate\Console\Command;
use Monolog\Logger;
use Monolog\Handler\RotatingFileHandler;

class RemovedExpiredLinkCommand extends Command
{
	private $log = null;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'File:removeExpiredFile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Will remove expire link and files from application.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
	    $this->log = new Logger('name');
	    $this->log->pushHandler(new RotatingFileHandler(storage_path().'/logs/file-delete-log',2, Logger::INFO));

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fileCollection = UploadFile::all();
	    foreach($fileCollection as $file) {
		    $toDate = strtotime('NOW');
		    $expireDate = strtotime($file->created_at. ' + '.env('LINK_EXPIRE', 10). ' days');

		    if($toDate > $expireDate) {
		    	$filePath = storage_path('app/'.$file->upload_path);
			    @unlink($filePath);
			    $file->delete();

		    }
	    }

    }
}
