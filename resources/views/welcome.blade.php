@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
                <form action="/file-upload" class="dropzone" method="post">
                    {{ csrf_field() }}
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                </form>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 show-links">

        </div>
    </div>

@endsection

@section('javascript')
    <script>
        Dropzone.autoDiscover = false;
        // or disable for specific dropzone:
        // Dropzone.options.myDropzone = false;

        $(function() {
            // Now that the DOM is fully loaded, create the dropzone, and setup the
            // event listeners
            var myDropzone = new Dropzone(".dropzone");
            myDropzone.on("complete", function(file) {
                myDropzone.removeFile(file);

                if(file.xhr.status == 200) {
                    var data = {
                        alert: 'alert-success',
                        message: 'uploaded successfully'
                    }

                    var response = jQuery.parseJSON(file.xhr.response);
                    var showLinkTemplate = 'Share this download link: <br /> ' + response.link;
                    jQuery('.show-links').html(showLinkTemplate);

                } else {
                    var data = {
                        alert: 'alert-danger',
                        message: file.xhr.response
                    }
                }

                jQuery('.alert-message').remove();

                var htmlTemplateAlert = '<div class="col-md-12 alert-message">'+
                        '<div class="alert '+data.alert+'">'+
                        data.message +
                        '</div>'+
                        '</div>';

                jQuery('#upload-message').append(htmlTemplateAlert);

                console.log(file.xhr);
                console.log(file.xhr.response);
            });
        })
    </script>


@endsection
